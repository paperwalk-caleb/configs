# SSH shortcuts
# ! don't do this :)
alias mc='echo "9^2EJ#rdm*U@Tc" | pbcopy; ssh user@192.168.1.35'

# profile conveniences
alias sz='source ~/.zshrc'
alias va='nvim ~/.aliases.sh'
alias vz='nvim ~/.zshrc'

# rm shorthands
alias rmf='rm -rf'
alias rmimg='rm *.png *.jpg *.jpeg *.webm'

# navigation
alias cdd='cd ~/Desktop'
alias cdw='cd ~/Downloads'
alias cds='cd ~/Scripts'
alias cdp='cd ~/Workspace'

# ls aliases
alias cl='clear; ls'
alias cll='clear; ls -l'

# launchers
alias crd='/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome https://remotedesktop.google.com/access/'
alias finance='python3 ~/Scripts/finance.py'
alias messages='python3 ~/Scripts/messages.py'
alias wurst='python3 ~/Scripts/wurst7.py'
alias brew='arch -x86_64 brew'
alias firefox='/Applications/Firefox.app/Contents/MacOS/firefox'
alias py='python3'

# daemon revive/kill
alias start-crd='launchctl start org.chromium.chromoting'
alias stop-crd='launchctl stop org.chromium.chromoting'
